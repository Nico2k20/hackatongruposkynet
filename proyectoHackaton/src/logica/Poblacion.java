package logica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Poblacion {

	private int cantidadDeModelos = 50;
	private int mutacionesPorIteracion = 20;
	private int recombinacionesPorIteracion = 20;
	private int eliminadorPorIteracion = 15;
	private int iteracionesTope = 250;
	private int cantMaxObjetos = 200;
	private BigDecimal umbralCosto= new BigDecimal("4000.00");
	private ArrayList<Individuo> individuos;
	private ArrayList<Objeto> objetos = new ArrayList<Objeto>(cantMaxObjetos);
	private ArrayList <Individuo> mejoresIndividuos;
	private Generador generador;

	Poblacion(Generador generador) {
		Random rand = new Random();
		
		this.generador = generador;
		this.individuos = new ArrayList<Individuo>(this.cantidadDeModelos);
		this.mejoresIndividuos = new ArrayList<Individuo>(150);
		for (int i = 0; i < cantMaxObjetos; i++) {

			objetos.add(new Objeto(i, i * 100, rand.nextInt(50), rand.nextInt(50), rand.nextInt(50)));

		}

	}

	public void generarModelos() {

		crearIndividuos(this.objetos);

		for (int i = 0; i < iteracionesTope; i++) {

			realizarFitness();
			mutarModelos();
			recombinarModelos();
			eliminarModelos();
			descartarModelo();
			adjuntarMejores();
			agregarNuevosIndividuos();
			this.mostrarResultados();
		}

		Collections.sort(this.mejoresIndividuos,(p, q) -> {

			if (p.getBigDecimal().compareTo(q.getBigDecimal()) == 1)
				return 1;
			else if ((p.getBigDecimal().compareTo(q.getBigDecimal())) == 0)
				return 0;
			else
				return -1;});
	}

	private void descartarModelo() {
		for (int j = 0; j < this.individuos.size(); j++) {
			if (this.individuos.get(j).EstaElModeloDescartado())
				this.individuos.remove(j);

		}

	}

	private void crearIndividuos(ArrayList<Objeto> objetos2) {

		for (int i = 0; i < this.cantidadDeModelos; i++) {

			this.individuos.add(Individuo.crearIndividuoAleatorio(generador, objetos2));
		}

	}

	private void mostrarResultados() {
		Collections.sort(this.individuos, (p, q) -> {

			if (p.getBigDecimal().compareTo(q.getBigDecimal()) == 1)
				return 1;
			else if ((p.getBigDecimal().compareTo(q.getBigDecimal())) == 0)
				return 0;
			else
				return -1;
		});

		/* Peor individuo */
		System.out.print("Peor Individuo" + this.individuos.get(this.individuos.size() - 1).fitness().toString());

		/* Mejor individuo */
		System.out.println("----MejorIndividuo" + this.individuos.get(0).fitness().toString() + "NO ES SOLUCION : "
				+ this.individuos.get(0).EstaElModeloDescartado() + ":"
				+ this.individuos.get(0).getObjetosNoEmpaquetados());
		
		
	}
	private void adjuntarMejores() {
		if (this.individuos.get(0).EstaElModeloDescartado()==false)
			this.mejoresIndividuos.add(this.individuos.get(0));
	}
	

	private void realizarFitness() {
		for (int i = 0; i < this.individuos.size(); i++) {
			this.individuos.get(i).fitness();
		}

	}

	private void mutarModelos() {
		for (int i = 0; i < this.mutacionesPorIteracion; i++) {
			mutarIndividuoAleatorio().mutacion();
			;

		}

	}

	private Individuo mutarIndividuoAleatorio() {
		Random rand = new Random();
		int indiceIndividuo = rand.nextInt(this.individuos.size());

		return this.individuos.get(indiceIndividuo);

	}

	private void agregarNuevosIndividuos() {
		for (int i = this.individuos.size(); i < this.cantidadDeModelos; i++) {
			this.individuos.add(Individuo.crearIndividuoAleatorio(this.generador, this.objetos));

		}

	}

	private void eliminarModelos() {
		Collections.sort(this.individuos, (p, q) -> {

			if (p.getBigDecimal().compareTo(q.getBigDecimal()) == 1)
				return 1;
			else if ((p.getBigDecimal().compareTo(q.getBigDecimal())) == 0)
				return 0;
			else
				return -1;
		});

		Collections.reverse(this.individuos);

		for (int i = 0; i <= this.eliminadorPorIteracion; i++) {
			this.individuos.remove(i);

		}

	}

	
	private void recombinarModelos() {
		for (int i = 0; i < this.recombinacionesPorIteracion; i++) {

			Individuo[] arrInd = mutarIndividuoAleatorio().combinar(mutarIndividuoAleatorio(), objetos);

			for (int j = 0; j < arrInd.length; j++) {
				this.individuos.add(arrInd[j]);

			}
		}

	}
	
	public ArrayList<Individuo> getMejoresModelos(){
		
		return	this.mejoresIndividuos;
	}

}
