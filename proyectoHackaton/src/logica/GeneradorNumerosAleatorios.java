package logica;

import java.util.Random;

public class GeneradorNumerosAleatorios implements Generador{

	private  Random random;
	
	
	public GeneradorNumerosAleatorios() {
		
		this.random = new Random();
	}
	
	@Override
	public boolean generarBoolean() {
		
		return random.nextBoolean();
	}

	@Override
	public int generarEntero(int rango) {
		// TODO Auto-generated method stub
		return random.nextInt(rango);
	}

}
