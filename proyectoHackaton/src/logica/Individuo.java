package logica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

public class Individuo {

	private boolean[] genoma1;
	private boolean[] genoma2;
	private boolean[] genoma3;
	private int objetosNoEmpaquetados;
	private Generador generador;
	private int semillaGeneradorMutacion;
	private BigDecimal sumatoriaCosto;
	private ArrayList<Objeto> objetosAComparar;
	private boolean modeloDescartado;

	public Individuo(Generador generador, ArrayList<Objeto> objetosBD) {

		this.generador = generador;
		this.genoma1 = new boolean[] { true, false, false, false, false, false, false };
		this.genoma2 = new boolean[] { true, false, false, false, false, false, false };
		this.genoma3 = new boolean[] { true, false, false, false, false, false, false };
		this.objetosAComparar = objetosBD;
		this.semillaGeneradorMutacion = this.genoma1.length - 1;
		this.objetosNoEmpaquetados = 0;
		this.sumatoriaCosto = new BigDecimal("00000.00000");
		modeloDescartado = false;

	}

	public static Individuo crearIndividuoAleatorio(Generador generador, ArrayList<Objeto> objetos) {
		Individuo nuevoIndividuo = new Individuo(generador, objetos);

		for (int i = 0; i < nuevoIndividuo.genoma1.length; ++i) {

			nuevoIndividuo.setBitGen1(i, generador.generarBoolean());
			nuevoIndividuo.setBitGen2(i, generador.generarBoolean());
			nuevoIndividuo.setBitGen3(i, generador.generarBoolean());
		}

		return nuevoIndividuo;
	}

	@SuppressWarnings("deprecation")
	public void mutacion() {
		int posicionAMutar = this.generador.generarEntero(semillaGeneradorMutacion);

		if (posicionAMutar < 4) {
			int posicionAMutar1 = this.generador.generarEntero(semillaGeneradorMutacion);
			int posicionAMutar2 = this.generador.generarEntero(semillaGeneradorMutacion);

			this.genoma1[posicionAMutar] = !(this.genoma1[posicionAMutar]);
			this.genoma2[posicionAMutar] = !(this.genoma2[posicionAMutar1]);
			this.genoma3[posicionAMutar] = !(this.genoma3[posicionAMutar2]);
		} else {
			boolean[] genomaX = this.genoma1;
			boolean[] genomaY = this.genoma2;
			boolean[] genomaZ = this.genoma3;
			this.genoma1 = genomaY;
			this.genoma2 = genomaZ;
			this.genoma1 = genomaX;
		}

	}

	public Individuo[] combinar(Individuo individuoCombinacion, ArrayList<Objeto> objetos) {
		int hastaDondeCombinar = this.generador.generarEntero(this.semillaGeneradorMutacion);

		Individuo individuo = new Individuo(this.generador, objetos);
		Individuo individuo1 = new Individuo(this.generador, objetos);

		/* Combinacion Gen 1 */
		for (int i = 0; i < hastaDondeCombinar; i++) {

			individuo.setBitGen1(i, this.genoma1[i]);
			individuo.setBitGen2(i, this.genoma2[i]);
			individuo.setBitGen3(i, this.genoma3[i]);

			individuo1.setBitGen1(i, this.genoma1[i]);
			individuo1.setBitGen2(i, this.genoma2[i]);
			individuo1.setBitGen3(i, this.genoma3[i]);
		}

		for (int i = hastaDondeCombinar; i <= this.semillaGeneradorMutacion; i++) {
			individuo.setBitGen1(i, this.genoma1[i]);
			individuo.setBitGen2(i, this.genoma2[i]);
			individuo.setBitGen3(i, this.genoma3[i]);

			individuo1.setBitGen1(i, this.genoma1[i]);
			individuo1.setBitGen2(i, this.genoma2[i]);
			individuo1.setBitGen3(i, this.genoma3[i]);

		}

		/* Combinacion Gen 2 */
		for (int i = 0; i < hastaDondeCombinar; i++) {
			individuo.setBitGen1(i, this.genoma1[i]);
			individuo.setBitGen2(i, this.genoma2[i]);
			individuo.setBitGen3(i, this.genoma3[i]);

			individuo1.setBitGen1(i, this.genoma1[i]);
			individuo1.setBitGen2(i, this.genoma2[i]);
			individuo1.setBitGen3(i, this.genoma3[i]);

		}

		for (int i = hastaDondeCombinar; i <= this.semillaGeneradorMutacion; i++) {
			individuo.setBitGen1(i, this.genoma1[i]);
			individuo.setBitGen2(i, this.genoma2[i]);
			individuo.setBitGen3(i, this.genoma3[i]);

			individuo1.setBitGen1(i, this.genoma1[i]);
			individuo1.setBitGen2(i, this.genoma2[i]);
			individuo1.setBitGen3(i, this.genoma3[i]);

		}

		/* Combinacion Gen 3 */
		for (int i = 0; i < hastaDondeCombinar; i++) {
			individuo.setBitGen1(i, this.genoma1[i]);
			individuo.setBitGen2(i, this.genoma2[i]);
			individuo.setBitGen3(i, this.genoma3[i]);

			individuo1.setBitGen1(i, this.genoma1[i]);
			individuo1.setBitGen2(i, this.genoma2[i]);
			individuo1.setBitGen3(i, this.genoma3[i]);

		}

		for (int i = hastaDondeCombinar; i <= this.semillaGeneradorMutacion; i++) {
			individuo.setBitGen1(i, this.genoma1[i]);
			individuo.setBitGen2(i, this.genoma2[i]);
			individuo.setBitGen3(i, this.genoma3[i]);

			individuo1.setBitGen1(i, this.genoma1[i]);
			individuo1.setBitGen2(i, this.genoma2[i]);
			individuo1.setBitGen3(i, this.genoma3[i]);

		}

		return new Individuo[] { individuo, individuo1 };

	}

	public BigDecimal fitness() {
		this.objetosNoEmpaquetados = 0;
		this.sumatoriaCosto = new BigDecimal("0.00000");
		this.modeloDescartado = false;
		int altoPaquete = 0;
		int anchoPaquete = 0;
		int profundidadPaquete = 0;
		int valorGenoma1 = AlgoritmosDecimales.traducirGenoma(genoma1);
		int valorGenoma2 = AlgoritmosDecimales.traducirGenoma(genoma2);
		int valorGenoma3 = AlgoritmosDecimales.traducirGenoma(genoma3);

		for (int i = 0; i < this.objetosAComparar.size(); i++) {

			altoPaquete = this.objetosAComparar.get(i).getAlto();
			anchoPaquete = this.objetosAComparar.get(i).getAncho();
			profundidadPaquete = this.objetosAComparar.get(i).getProfundidad();

			if ((valorGenoma1) - 1 > altoPaquete && (valorGenoma2) - 1 > anchoPaquete
					&& (valorGenoma3) - 1 > profundidadPaquete)
				this.sumatoriaCosto = this.sumatoriaCosto
						.add(AlgoritmosDecimales.calcularCosto(valorGenoma1, valorGenoma2, valorGenoma3));
			else
				this.objetosNoEmpaquetados++;

		}
		if (this.objetosNoEmpaquetados >= this.objetosAComparar.size() / 2
				|| this.sumatoriaCosto.compareTo(new BigDecimal("0.00000")) == 0) {
			this.modeloDescartado = true;

		}

		return this.sumatoriaCosto;

	}

	public StringBuilder guardarfitness() {
		this.objetosNoEmpaquetados = 0;
		this.sumatoriaCosto = new BigDecimal("0.00000");
		this.modeloDescartado = false;
		int altoPaquete = 0;
		int anchoPaquete = 0;
		int profundidadPaquete = 0;
		int idEnvio = 0;
		int cantidadDeCompras = 0;
		
		int valorGenoma1 = AlgoritmosDecimales.traducirGenoma(genoma1);
		int valorGenoma2 = AlgoritmosDecimales.traducirGenoma(genoma2);
		int valorGenoma3 = AlgoritmosDecimales.traducirGenoma(genoma3);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.objetosAComparar.size(); i++) {

			idEnvio =this.objetosAComparar.get(i).getIdProducto();
			cantidadDeCompras = this.objetosAComparar.get(i).getUnidadesVendidas();;
			altoPaquete = this.objetosAComparar.get(i).getAlto();
			anchoPaquete = this.objetosAComparar.get(i).getAncho();
			profundidadPaquete = this.objetosAComparar.get(i).getProfundidad();

			if ((valorGenoma1) - 1 > altoPaquete && (valorGenoma2) - 1 > anchoPaquete
					&& (valorGenoma3) - 1 > profundidadPaquete) {
				
				sb.append("|Alto envase "+valorGenoma1+" Ancho envase "+valorGenoma2
						+" Profundidad envase "+valorGenoma3
						+" id envio "+ idEnvio+" id envio "+" Cantidad de compras "+cantidadDeCompras 
						+"  Alto paquete "+ altoPaquete+"  Ancho paquete "
						+ anchoPaquete+"  profundidad paquete "+ profundidadPaquete+ this.sumatoriaCosto
						.add(AlgoritmosDecimales.calcularCosto(valorGenoma1, valorGenoma2, valorGenoma3)).toString()+" " );
				
				}
			else
				this.objetosNoEmpaquetados++;

		}
		
		sb.append("Costo total " + this.sumatoriaCosto.toString()+"|");

		return sb;

	}

	///////////////////////////////////////////// Setters ///////////
	/* Setter generador */
	public void setGenerador(Generador generador) {

		this.generador = generador;
	}

	/* Setters genes */
	private void setBitGen1(int posicion, boolean valor) {

		this.genoma1[posicion] = valor;
	}

	private void setBitGen2(int posicion, boolean valor) {
		this.genoma2[posicion] = valor;
	}

	private void setBitGen3(int posicion, boolean valor) {
		this.genoma3[posicion] = valor;
	}

	/* Getters genes */
	private boolean[] getGenen1() {
		return this.genoma1;

	}

	private boolean[] getGenen2() {
		return this.genoma2;

	}

	private boolean[] getGenen3() {
		return this.genoma3;

	}

	public BigDecimal getBigDecimal() {

		return this.sumatoriaCosto;

	}

	public int getObjetosNoEmpaquetados() {
		return this.objetosNoEmpaquetados;

	}

	public boolean EstaElModeloDescartado() {
		return modeloDescartado;
	}

	public String toString() {

		return " Genoma 1 " + AlgoritmosDecimales.traducirGenoma(genoma1) + " Genoma 2  "
				+ AlgoritmosDecimales.traducirGenoma(genoma2) + " genoma 3 "
				+ AlgoritmosDecimales.traducirGenoma(genoma3);
	}

}
