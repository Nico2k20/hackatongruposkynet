package logica;

public class Objeto {

	private int idProducto;
	private int unidadesVendidas;
	private int ancho;
	private int alto;
	private int profundidad;
	
	
	
	
	public Objeto(int id, int unidadesVendidas,int ancho,int alto, int profundidad) {
		
		this.setIdProducto(id);
		this.setUnidadesVendidas(unidadesVendidas);
		this.setAncho(ancho);
		this.setAlto(alto);
		this.setProfundidad(profundidad);
		}




	public int getIdProducto() {
		return idProducto;
	}




	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}




	public int getUnidadesVendidas() {
		return unidadesVendidas;
	}




	public void setUnidadesVendidas(int unidadesVendidas) {
		this.unidadesVendidas = unidadesVendidas;
	}




	public int getProfundidad() {
		return profundidad;
	}




	public void setProfundidad(int profundidad) {
		this.profundidad = profundidad;
	}




	public int getAlto() {
		return alto;
	}




	public void setAlto(int alto) {
		this.alto = alto;
	}




	public int getAncho() {
		return ancho;
	}




	public void setAncho(int ancho) {
		this.ancho = ancho;
	}
	
	public String toString() {
		return this.idProducto+" " + this.unidadesVendidas +" "+ this.alto+" "+ this.ancho+ " " +this.profundidad+"";
	}
}
