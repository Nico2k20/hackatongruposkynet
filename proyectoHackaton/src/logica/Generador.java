package logica;

public interface Generador {

	
	public boolean generarBoolean();
	public int generarEntero(int rango);
}
