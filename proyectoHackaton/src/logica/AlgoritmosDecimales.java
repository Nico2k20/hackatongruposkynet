package logica;

import java.math.BigDecimal;

public class AlgoritmosDecimales {

	private static BigDecimal constanteCaja = new BigDecimal("0.0001");
	private static BigDecimal constanteRelleno = new BigDecimal("0.00012");
	private static BigDecimal constanteTransporte1 = new BigDecimal("0.003");
	private static BigDecimal constanteTransporte2 = new BigDecimal("0.01");
	private static BigDecimal constanteTransporte3 = new BigDecimal("0.015");
	private static BigDecimal constFormulaTrans = new BigDecimal("15.00");
	private static BigDecimal constFormulaTrans2 = new BigDecimal("8.00");
	private static BigDecimal constFormulaTrans3 = new BigDecimal("10.00");

//	private static BigDecimal irracionalE = new BigDecimal(Math.E);

	@SuppressWarnings("deprecation")
	public static BigDecimal calcularCosto(int ancho, int alto, int profundidad) {
		int Cm3 = ancho * alto * profundidad;

		BigDecimal cm3Decimal = new BigDecimal(Cm3 + ".000");

		@SuppressWarnings("unused")
		BigDecimal constTransporte = new BigDecimal("0000.00000");

		/* rango 1 transporte */
		if (0 < Cm3 && Cm3 < 5000) {
			constTransporte = constanteTransporte1.multiply(cm3Decimal).add(constFormulaTrans);
		}
		/* rango 2 transporte */
		else if (5000 < Cm3 && Cm3 < 8000) {
			constanteTransporte2.multiply(cm3Decimal);
			constanteTransporte2.subtract(constFormulaTrans2);
			constTransporte = constanteTransporte1;
		}
		/* rango 3 transporte */
		else if (Cm3 > 8000) {
			constanteTransporte3.multiply(cm3Decimal);
			constanteTransporte3.subtract(constFormulaTrans3);
			constTransporte = constanteTransporte1;
		}

		constanteCaja.multiply(cm3Decimal);

		/* Falta elevar a e */
		BigDecimal retorno = constanteRelleno.multiply(cm3Decimal).add(constanteCaja.multiply(cm3Decimal))
				.add(constTransporte);


		return retorno;

	}

	static int traducirGenoma(boolean[] genoma) {
		int valor = 0;

		for (int i = 0; i < genoma.length; i++) {
			if (genoma[i] == true)
				valor += Math.pow(2, i);
		}

//		System.out.println(valor);
		return valor;
	}

}
